﻿Public Class MainForm

    Dim NeedToSleep As Boolean = False
    Dim myProcManager As New ProcessManager
    Dim DB As New DbManger
    Dim myNotificationSystem As New NotificationSystem
    Dim problematicProcesses As List(Of String)
    Dim log As String

    Public Sub Updater()
        problematicProcesses = myProcManager.ScanList()

        For Each proc In problematicProcesses
            'Double Check - just in case (IMPORTANT!)
            If myProcManager.HasWindow(proc) Then
                Continue For
            End If

            If My.Settings.AlwaysCloseList.Contains(proc) Then
                If myNotificationSystem.Notify(myNotificationSystem.YES_NO, "AutoClose", proc & " will automatically be terminated in a few seconds." & vbNewLine & "Do you want to cancel the close?", 11) Then
                    If myNotificationSystem.Notify(myNotificationSystem.YES_NO, "Remove From AutoClose List?", "Do you want to remove " & proc & " from the AutoClose List?") Then
                        My.Settings.AlwaysCloseList.Remove(proc)
                        My.Settings.Save()
                    End If
                Else
                    Dim procKillException As Exception = myProcManager.KillProcess(proc)
                    If Not IsNothing(procKillException) Then
                        myNotificationSystem.Notify(myNotificationSystem.OK, "ERROR!", procKillException.Message)
                    End If
                End If
                Continue For
            End If

            If myNotificationSystem.Notify(myNotificationSystem.YES_NO, "Process Found!", proc + " is still open in the background!" + vbNewLine + "Do you want to kill it?") Then
                'There is An exception in the KillProcess Function
                Dim procKillException As Exception = myProcManager.KillProcess(proc)
                If Not IsNothing(procKillException) Then
                    myNotificationSystem.Notify(myNotificationSystem.OK, "ERROR!", procKillException.Message)
                Else
                    If myNotificationSystem.Notify(myNotificationSystem.YES_NO, "Sucessfully terminated!", "Do you want to terminate it every time its staying in the background?") Then
                        My.Settings.AlwaysCloseList.Add(proc)
                        My.Settings.Save()
                    End If
                End If
            Else
                'if he dosent want to kill it maybe he wants to whitelist it
                If myNotificationSystem.Notify(myNotificationSystem.YES_NO, "Whitelist Process?", "Do you want to whitelist " + proc + " ?") Then
                    'adding to whitelist
                    My.Settings.whiteList.Add(proc)
                    My.Settings.Save()
                End If
            End If
        Next
    End Sub

    Public Sub HideForm()
        'hide from
        Me.WindowState = FormWindowState.Minimized

        'Try
        '    Me.ShowInTaskbar = False
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Application.EnableVisualStyles()
        'show tray icon
        TrayIcon.ShowBalloonTip(10000)
        'disabing resume option, the software didnt stop yet
        ResumeToolStripMenuItem.Enabled = False
        HideForm()
        'refreshing the open processes list once every one second - to be updated even with programs that was opened for a short while
        AutoRefreshList.Start()
        'but checking them every 4 seconds for a couple of reasons: first - it might take a second or two for a process to normally close, second - reducing the cpu usage.
        CheckProblematicProcesses.Start()
    End Sub

    Private Sub AutoRefresh_Tick(sender As Object, e As EventArgs) Handles AutoRefreshList.Tick
        'The DGV is cpu intensive and taking 5-6% more cpu usage than the normal program usage (aroud 5%) - dont forget than in the final prod they wont be any constant GUI activity.
        'statusLabel.Text = "Refreshing..."
        'myProcManager.RefreshDGV(ProcessesDGV)
        If Not BW_AutoRefresh.IsBusy() Then
            BW_AutoRefresh.RunWorkerAsync()
        End If
        'statusLabel.Text = "Refreshed Successfully!"
    End Sub

    Private Sub CheckProblematicProcesses_Tick(sender As Object, e As EventArgs) Handles CheckProblematicProcesses.Tick
        If Not BW_ProblematicProsesses.IsBusy Then
            BW_ProblematicProsesses.RunWorkerAsync()
        End If
    End Sub

    Public Sub CheckCrucialCpuUsage()
        Dim cpu As New PerformanceCounter()
        With cpu
            .CategoryName = "Processor"
            .CounterName = "% Processor Time"
            .InstanceName = "_Total"
        End With

        'according to MSDN - "The method nextValue() always returns a 0 value on the first call. So you have to call this method a second time." - LINK: http://msdn.microsoft.com/en-us/library/system.diagnostics.performancecounter.aspx
        cpu.NextValue()
        'wating a second because the cpu usage is relative to the second before - see documentation
        Threading.Thread.Sleep(1000)

        If cpu.NextValue() > My.Settings.CrucialCpuUse + 5 Then
            myProcManager.NoWindowCrcialCpu()
        End If
    End Sub

    Private Sub ExitMenuItem_Click(sender As Object, e As EventArgs) Handles ExitMenuItem.Click
        'nicely removing the tray icon (it will be gone anyway when the app is closed but we dont want to leave mess)
        TrayIcon.Dispose()
        'closing the App
        Me.Close()
    End Sub

    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        'show main form
        DB.ReadDB()
        Me.WindowState = FormWindowState.Normal
        Me.ShowInTaskbar = True
    End Sub

    Private Sub SettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem.Click
        Dim newSettingsWindow As New Settings
        newSettingsWindow.ShowDialog()
    End Sub

    Private Sub MainForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If My.Settings.MinimizeToTrayOnClose Then
            e.Cancel = True
            HideForm()
            Exit Sub
        End If
        Try
            TrayIcon.Visible = False
            TrayIcon.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SleepProgramTimer_Tick(sender As Object, e As EventArgs) Handles SleepProgramTimer.Tick
        NeedToSleep = False
        ResumeToolStripMenuItem.Enabled = False
        SleepProgramTimer.Stop()
    End Sub

    Private Sub MinuteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MinuteToolStripMenuItem.Click
        SleepProgram(1)
    End Sub

    Public Sub SleepProgram(Minutes As Integer)
        NeedToSleep = True
        'Enabling resume option
        ResumeToolStripMenuItem.Enabled = True
        SleepProgramTimer.Interval = TimeSpan.FromMinutes(Minutes).TotalMilliseconds
        SleepProgramTimer.Start()
    End Sub

    Private Sub MinutesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MinutesToolStripMenuItem.Click
        SleepProgram(15)
    End Sub

    Private Sub HourToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HourToolStripMenuItem.Click
        SleepProgram(60)
    End Sub

    Private Sub CustomToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomToolStripMenuItem.Click
        NeedToSleep = True
    End Sub

    Private Sub ResumeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResumeToolStripMenuItem.Click
        NeedToSleep = False
        ResumeToolStripMenuItem.Enabled = False
        SleepProgramTimer.Stop()
    End Sub

    Private Sub BW_AutoRefresh_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BW_AutoRefresh.DoWork
        myProcManager.RefreshList()
    End Sub

    Private Sub BW_ProblematicProsesses_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BW_ProblematicProsesses.DoWork
        Updater()
        CheckCrucialCpuUsage()
    End Sub

    Private Sub BW_ProblematicProsesses_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BW_ProblematicProsesses.RunWorkerCompleted
        log += vbNewLine & TimeOfDay.ToString("h:mm:ss.fff") & " - ProbelmaticScan_End "
    End Sub

    Private Sub BW_AutoRefresh_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BW_AutoRefresh.RunWorkerCompleted
        log += vbNewLine & TimeOfDay.ToString("h:mm:ss.fff") & " - AutoRefresh_End "
    End Sub

    Private Sub CheckedListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox1.SelectedIndexChanged
        Dim i As Integer = 0
        If CheckedListBox1.CheckedItems.Count > 0 Then
            Me.Label4.Visible = False
        End If
        DB.checkNames.Clear()
        For Each NameExist In CheckedListBox1.CheckedItems
            DB.checkNames.Add(NameExist.ToString())
        Next
        DB.createThirdTable()
    End Sub

End Class
