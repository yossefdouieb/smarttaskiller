﻿Imports System.Data.SQLite

Public Class DbManger
    Private _dictionary As Dictionary(Of String, List(Of Double))
    Public checkNames As List(Of String) = New List(Of String)
    Public thirdChart As Dictionary(Of String, List(Of Double)) = New Dictionary(Of String, List(Of Double))
    Private names As Dictionary(Of String, Boolean) = New Dictionary(Of String, Boolean)
    Private colorDict As Dictionary(Of String, Color) = New Dictionary(Of String, Color)


    Public Sub ReadDB()
        Dim sqlite_conn As SQLiteConnection = Nothing
        Dim sqlite_cmd As SQLiteCommand = Nothing
        Dim sqlite_datareader As SQLiteDataReader = Nothing

        InitiateDB(sqlite_conn, sqlite_cmd, sqlite_datareader)

        sqlite_cmd.CommandText = "SELECT * FROM database;"
        sqlite_datareader = sqlite_cmd.ExecuteReader()

        Dim AppList As List(Of String) = New List(Of String)
        Dim CpuList As List(Of Double) = New List(Of Double)
        Dim RamList As List(Of Double) = New List(Of Double)
        Dim TimeList As List(Of DateTime) = New List(Of DateTime)

        ' The SQLiteDataReader allows us to run through each row per loop
        While (sqlite_datareader.Read()) ' Read() returns true if there is still a result line to read

            ' Print out the content of the text field:
            ' Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'")
            Dim textReader As String = ""
            Dim cpuReader As Double = 0
            Dim ramReader As Double = 0
            Dim timeReader As DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond)

            textReader = sqlite_datareader.GetString(0)
            cpuReader = sqlite_datareader.GetDouble(1)
            ramReader = sqlite_datareader.GetDouble(2)
            timeReader = sqlite_datareader.GetDateTime(3)

            AppList.Add(textReader)
            CpuList.Add(cpuReader)
            RamList.Add(ramReader)
            TimeList.Add(timeReader)

            'OutputTextBox.Text = OutputTextBox.Text + "Name =  '" + textReader + "' CPU = " + cpuReader.ToString + " RAM = " + ramReader.ToString + " " + Environment.NewLine

        End While
        'Dim i As Integer = 0
        'Do Until i = AppList.Count
        '    MainForm.TextBox1.Text = MainForm.TextBox1.Text + "Name =  '" & AppList.ElementAt(i) & "' CPU = " & CpuList.ElementAt(i) & " RAM = " & RamList.ElementAt(i) & " " & "Time = " & TimeList.ElementAt(i) & Environment.NewLine
        '    'Chart1.Series("cpu").Points.AddXY(AppList.ElementAt(i), CpuList.ElementAt(i))
        '    'Chart1.Series("ram").Points.AddXY(AppList.ElementAt(i), RamList.ElementAt(i))
        '    i += 1
        'Loop
        sqlite_conn.Cancel()
        If Not AppList.Count = 0 Then
            MainForm.Label1.Visible = False
            MainForm.Label2.Visible = False
            MainForm.Label3.Visible = False
        End If
        Dim db As Dictionary(Of String, List(Of Double)) = calc(AppList, CpuList, RamList, TimeList)
        For Each pair In db
            Dim t As List(Of Double) = pair.Value
            'TextBox1.Text = TextBox1.Text + "Name =  '" & pair.Key & "' CPU = " & t(0) & " RAM = " & t(1) & " " & Environment.NewLine
        Next
        Me._dictionary = db
        MainForm.CpuChart.Series("cpu").Points.Clear()
        MainForm.RamChart.Series("ram").Points.Clear()

        For Each pair In _dictionary
            Dim t As List(Of Double) = pair.Value
            If t(0) <> 0.0 Then
                MainForm.CpuChart.Series("cpu").Points.AddXY(pair.Key, t(0))
            End If
            MainForm.RamChart.Series("ram").Points.AddXY(pair.Key, t(1))
            MainForm.RamChart.ChartAreas(0).AxisX.LabelStyle.Interval = 1
            MainForm.CpuChart.ChartAreas(0).AxisX.LabelStyle.Interval = 1
        Next
    End Sub

    Private Function calc(app As List(Of String), cpu As List(Of Double), ram As List(Of Double), time As List(Of DateTime)) As Dictionary(Of String, List(Of Double))
        Dim i As Integer = 0
        Dim db As New Dictionary(Of String, List(Of Double))
        Dim count As New Dictionary(Of String, Integer)
        Do Until i = app.Count
            Dim temp As List(Of Double) = New List(Of Double)
            temp.Add(cpu.ElementAt(i))
            temp.Add(ram.ElementAt(i))
            thirdChartCalc(app.ElementAt(i), temp(0), time.ElementAt(i))
            If db.ContainsKey(app.ElementAt(i)) Then
                ' Write value of the key.
                temp = db.Item(app.ElementAt(i))
                temp(0) += (cpu.ElementAt(i))
                temp(1) += (ram.ElementAt(i))
                db(app.ElementAt(i)) = temp
            Else
                db.Add(app.ElementAt(i), temp)
                If Not MainForm.CheckedListBox1.Items.Contains(app.ElementAt(i)) Then
                    MainForm.CheckedListBox1.Items.Add(app.ElementAt(i), False)
                End If
            End If

            If count.ContainsKey(app.ElementAt(i)) Then
                count(app.ElementAt(i)) = count(app.ElementAt(i)) + 1
            Else
                count.Add(app.ElementAt(i), 1)
            End If
            i += 1
        Loop
        Dim dbt As New Dictionary(Of String, List(Of Double))
        For Each pair In db
            Dim t As List(Of Double) = pair.Value
            t(0) = t(0) / count(pair.Key)
            t(1) = t(1) / count(pair.Key)
            dbt(pair.Key) = t
        Next
        Return dbt
    End Function

    Public Sub writeDB(name As String, cpu As Byte, ram As Double)
        Dim sqlite_conn As SQLiteConnection = Nothing
        Dim sqlite_cmd As SQLiteCommand = Nothing
        Dim sqlite_datareader As SQLiteDataReader = Nothing

        InitiateDB(sqlite_conn, sqlite_cmd, sqlite_datareader)

        Dim cpu1 As Double = cpu
        Dim ram1 As Double = ram / 1073741824.0
        ram1 = ram1 * 1024.0

        sqlite_cmd.CommandText = "INSERT INTO database (APP,CPU,RAM) VALUES ('" + name + "'," + cpu1.ToString() + "," + ram1.ToString() + ");"
        sqlite_cmd.ExecuteNonQuery()
        sqlite_conn.Cancel()
    End Sub

    Public Sub createThirdTable()
        Dim name As String = ""


        'Static Dim j As Integer = 0
        'Dim c1 As New DataVisualization.Charting.Series("Series 1")
        'Dim c2 As New DataVisualization.Charting.Series("Series 2")
        'Dim c3 As New DataVisualization.Charting.Series("Series 3")
        'If j > 2 Then
        '    c1 = MainForm.TimeChart.Series(checkNames.ElementAt(0))
        '    c2 = MainForm.TimeChart.Series(checkNames.ElementAt(1))
        '    c3 = MainForm.TimeChart.Series(checkNames.ElementAt(2))
        'Else
        '    j = j + 1
        'End If

        MainForm.TimeChart.Series.Clear()
        For Each name In checkNames
            MainForm.TimeChart.Series.Add(name)

            MainForm.TimeChart.Series(name).ChartType = DataVisualization.Charting.SeriesChartType.Spline
            MainForm.TimeChart.Series(name).BorderWidth = 3

            'MainForm.TimeChart.ChartAreas(0).AxisX.ScaleView.Zoomable = True
            'MainForm.TimeChart.ChartAreas(0).CursorX.AutoScroll = True
            'MainForm.TimeChart.ChartAreas(0).CursorX.IsUserSelectionEnabled = True
            For i As Integer = 0 To thirdChart(name).Count - 1
                MainForm.TimeChart.Series(name).Points.AddY(thirdChart(name).ElementAt(i))
                If Not colorDict.ContainsKey(name) Then
                    Dim blue As Integer = Int((256 * Rnd()))
                    Dim red As Integer = Int((256 * Rnd()))
                    Dim green As Integer = Int((256 * Rnd()))
                    colorDict.Add(name, Color.FromArgb(red, green, blue))
                    'colorDict.Add(name, MainForm.TimeChart.Series(name).ShadowColor)

                End If
                MainForm.TimeChart.Series(name).Color = colorDict.Item(name)

            Next
        Next

    End Sub


    Private Sub InitiateDB(ByRef sqlite_conn As SQLiteConnection, ByRef sqlite_cmd As SQLiteCommand, ByRef sqlite_datareader As SQLiteDataReader)
        If Not System.IO.File.Exists("DB.db") Then
            sqlite_conn = New SQLiteConnection("Data Source=DB.db;Version=3;")
            sqlite_conn.Open()
            sqlite_cmd = sqlite_conn.CreateCommand()
            sqlite_cmd.CommandText = "CREATE TABLE database (APP TEXT, CPU DOUBLE, RAM DOUBLE, Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP);"
            sqlite_cmd.ExecuteNonQuery()
        Else
            sqlite_conn = New SQLiteConnection("Data Source=DB.db;Version=3;")
            sqlite_conn.Open()

            sqlite_cmd = sqlite_conn.CreateCommand()
        End If
    End Sub

    Private Sub thirdChartCalc(name As String, cpu As Double, time As DateTime)
        Dim temp As List(Of Double) = New List(Of Double)
        Static Dim count As Integer = 0
        Static Dim old_time As DateTime = time

        If (old_time.AddSeconds(3)) < time Then
            For Each n In names.Keys.ToList()
                If Not names(n) Then
                    temp = thirdChart(n)
                    temp.Add(0)
                    thirdChart(n) = temp
                End If
            Next
            For Each na In names.Keys.ToList()
                names(na) = False
            Next

            count += 1
            old_time = time

        End If

        If thirdChart.ContainsKey(name) Then
            temp = thirdChart(name)
            temp.Add(cpu)
            thirdChart(name) = temp

            names(name) = True
        Else
            For i As Integer = 0 To count - 1
                temp.Add(0)
            Next
            temp.Add(cpu)
            thirdChart.Add(name, temp)
        End If

        If Not names.ContainsKey(name) Then

            names.Add(name, True)
        Else
            names(name) = True
        End If
    End Sub

End Class
