﻿Imports System.Management

Public Class ProcessManager

    Dim MyPerformanceCounter As PerformanceCounter
    Dim DB As New DbManger
    Dim WindowedProcesses As New List(Of String)
    Dim RefreshingList As Boolean = False
    Dim ScanningList As Boolean = False
    Private TolleratedCpuUse As Byte = My.Settings.TolleratedCpuUse

    Dim searcher As ManagementObjectSearcher

    Private Declare Auto Function FindWindow Lib "user32.dll" (ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr

    Public Sub RefreshDGV(ToUpdate As DataGridView)
        ToUpdate.Rows.Clear()
        For Each p As Process In Process.GetProcesses
            Try
                ToUpdate.Rows.Add(p.ProcessName, p.Id, p.UserProcessorTime.ToString, (p.PrivateMemorySize64 / 1024 ^ 2).ToString + "MB", p.MainWindowTitle)
            Catch ex As Exception
                'in case of reading cpu or ram usage from system process which need system priviliges - no need to notify
            End Try
        Next

        'sorting the DGV to see the processes with window first
        ToUpdate.Sort(ToUpdate.Columns(4), ComponentModel.ListSortDirection.Descending)
    End Sub

    Public Sub RefreshList()
        If Not ScanningList Then
            RefreshingList = True
            For Each p As Process In Process.GetProcesses
                'if the process has a window
                Dim name As String = p.MainWindowTitle
                If p.MainWindowTitle.Length > 0 Then
                    'if we dont have in watch list already
                    If Not WindowedProcesses.Contains(p.ProcessName) Then
                        WindowedProcesses.Add(p.ProcessName)
                    End If
                End If
            Next

            RefreshingList = False
        End If
    End Sub


    Public Function HasWindow(ProcName As String) As Boolean
        Dim NewProc As Process = GetParentProcess(ProcName)
        Dim HasWindowBool As Boolean = False
        If NewProc IsNot Nothing Then
            If NewProc.MainWindowTitle.Length > 0 Then
                HasWindowBool = True
            End If
        End If
        For Each proc As Process In Process.GetProcessesByName(ProcName)
            If proc.MainWindowTitle.Length > 0 Then
                HasWindowBool = True
                Return HasWindowBool
            End If
        Next

        Return HasWindowBool
    End Function

    Public Function GetParentProcess(ProcName As String) As Process
        Try
            Dim performanceCounter As New PerformanceCounter("Process", "Creating Process ID", Process.GetProcessesByName(ProcName).ElementAt(0).ProcessName)
            Return Process.GetProcessById(Convert.ToInt32(performanceCounter.RawValue))
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function GetCpuUsage(ProcName As String) As Byte
        Try
            Dim cpuUsage As Double = 0
            Try
                searcher = New ManagementObjectSearcher("root\CIMV2", "SELECT * FROM Win32_PerfFormattedData_PerfProc_Process WHERE Name LIKE """ + ProcName + "%""")

                For Each queryObj As ManagementObject In searcher.[Get]()
                    Dim procesName = queryObj("Name").ToString

                    If procesName = procesName Then
                        cpuUsage += queryObj("PercentProcessorTime").ToString
                    End If
                Next

            Catch e As ManagementException
                MessageBox.Show("An error occurred while querying for WMI data: " & e.Message)
            End Try
            Return cpuUsage
        Catch ex As Exception
            ex.Message.ToString()
        End Try
        Return 0
    End Function

    'Public Function GetVisibleTrayProgramsPIDs() As List(Of Integer)
    '    Dim PIDList As List(Of Integer) = New List(Of Integer)

    '    Dim tskBarClassName As String = "Shell_TrayWnd"
    '    Dim tskBarHwnd As IntPtr = FindWindow(tskBarClassName, Nothing)
    '    Dim window As AutomationElement = AutomationElement.FromHandle(tskBarHwnd)
    '    Dim condition As New PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ToolBar)
    '    Dim elementCollection As AutomationElementCollection = window.FindAll(TreeScope.Descendants, condition)

    '    For Each aE As AutomationElement In elementCollection
    '        If aE.Current.Name.Equals("User Promoted Notification Area") Then
    '            For Each ui As AutomationElement In aE.FindAll(TreeScope.Descendants, New PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ToolBar))
    '                PIDList.Add(ui.Current.ProcessId)
    '            Next
    '        End If
    '    Next

    '    Return PIDList
    'End Function

    Public Function GetRam(ProcessName As String) As Long
        Dim totalRam As Long = 0
        For Each proc In Process.GetProcessesByName(ProcessName)
            totalRam += proc.PrivateMemorySize64
        Next
        Return totalRam
    End Function

    Public Function ScanList() As List(Of String)
        If Not RefreshingList Then
            ScanningList = True
            Dim TempList As List(Of String) = WindowedProcesses
            Dim TempProc() As Process
            Dim flag As Boolean = True
            Dim problematicProcesses As List(Of String) = New List(Of String)
            Dim newList As New List(Of String)

            For Each procName In TempList
                Dim CurrCpuUsage As Byte = GetCpuUsage(procName)
                Dim CurrRamUsage = GetRam(procName)
                DB.writeDB(procName, CurrCpuUsage, CurrRamUsage)
                If CurrCpuUsage < TolleratedCpuUse Then
                    Continue For
                End If
                flag = Not HasWindow(procName)
                TempProc = Process.GetProcessesByName(procName)
                If TempProc.Count > 0 Then

                    If flag Then
                        If Not My.Settings.whiteList.Contains(procName) Then
                            problematicProcesses.Add(procName)
                        End If
                    Else
                        newList.Add(procName)
                    End If
                End If
            Next

            WindowedProcesses = newList
            ScanningList = False
            Return problematicProcesses
        Else
            Return New List(Of String)
        End If
    End Function

    'finding processes that never had window but still using a lot of CPU
    Public Sub NoWindowCrcialCpu()
        Dim scannedProcesses As New List(Of String)
        For Each proc In Process.GetProcesses
            If Not proc.MainWindowTitle.Length > 0 Then
                'if the owner of the process is not the user it is probably the system
                'If Not GetUserName(proc.ProcessName) = Environment.UserName Then
                '    Continue For
                'End If

                If Not scannedProcesses.Contains(proc.ProcessName) Then
                    scannedProcesses.Add(proc.ProcessName)
                    If GetCpuUsage(proc.ProcessName) >= My.Settings.CrucialCpuUse Then
                        Debug.WriteLine(proc.ProcessName & " is using a lot of CPU!")
                    End If
                End If
            End If
        Next
    End Sub

    Public Function GetUserName(ByVal ProcessName As String) As String
        Dim selectQuery As SelectQuery = New SelectQuery("SELECT * FROM Win32_Process WHERE Name LIKE """ + ProcessName + "%""")
        searcher = New ManagementObjectSearcher(selectQuery)
        Dim y As ManagementObjectCollection
        y = searcher.Get
        For Each proc As ManagementObject In y
            Dim s(1) As String
            proc.InvokeMethod("GetOwner", CType(s, Object()))
            Dim n As String = proc("Name").ToString()
            If n = ProcessName & ".exe" Then
                Return s(0)
            End If
        Next
        Return ""
    End Function

    'Recive Process name to kill Returns Exception.
    Public Function KillProcess(ProcName As String) As Exception
        Dim killProcException As Exception = Nothing
        Dim ToKillProcess() As Process
        ToKillProcess = Process.GetProcessesByName(ProcName)
        For Each p As Process In ToKillProcess
            Try
                p.Kill()
            Catch ex As Exception
                killProcException = ex
            End Try
        Next
        Return killProcException
    End Function
End Class