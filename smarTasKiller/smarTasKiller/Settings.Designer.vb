﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.WhiteListLabel = New System.Windows.Forms.Label()
        Me.EditWhiteList = New System.Windows.Forms.LinkLabel()
        Me.EditAutoCloseList = New System.Windows.Forms.LinkLabel()
        Me.AutoCloseLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TollerableCpuUsageNumeric = New System.Windows.Forms.NumericUpDown()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CrucialCpuUsageNumeric = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.MinimizeRadioButton = New System.Windows.Forms.RadioButton()
        Me.CloseRadioButton = New System.Windows.Forms.RadioButton()
        Me.StartupCheckBox = New System.Windows.Forms.CheckBox()
        CType(Me.TollerableCpuUsageNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.CrucialCpuUsageNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "White List:"
        '
        'WhiteListLabel
        '
        Me.WhiteListLabel.AutoEllipsis = True
        Me.WhiteListLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.WhiteListLabel.Location = New System.Drawing.Point(82, 9)
        Me.WhiteListLabel.Name = "WhiteListLabel"
        Me.WhiteListLabel.Size = New System.Drawing.Size(206, 17)
        Me.WhiteListLabel.TabIndex = 1
        '
        'EditWhiteList
        '
        Me.EditWhiteList.AutoSize = True
        Me.EditWhiteList.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.EditWhiteList.Location = New System.Drawing.Point(294, 9)
        Me.EditWhiteList.Name = "EditWhiteList"
        Me.EditWhiteList.Size = New System.Drawing.Size(32, 17)
        Me.EditWhiteList.TabIndex = 2
        Me.EditWhiteList.TabStop = True
        Me.EditWhiteList.Text = "Edit"
        Me.EditWhiteList.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'EditAutoCloseList
        '
        Me.EditAutoCloseList.AutoSize = True
        Me.EditAutoCloseList.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.EditAutoCloseList.Location = New System.Drawing.Point(294, 37)
        Me.EditAutoCloseList.Name = "EditAutoCloseList"
        Me.EditAutoCloseList.Size = New System.Drawing.Size(32, 17)
        Me.EditAutoCloseList.TabIndex = 5
        Me.EditAutoCloseList.TabStop = True
        Me.EditAutoCloseList.Text = "Edit"
        Me.EditAutoCloseList.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'AutoCloseLabel
        '
        Me.AutoCloseLabel.AutoEllipsis = True
        Me.AutoCloseLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.AutoCloseLabel.Location = New System.Drawing.Point(120, 37)
        Me.AutoCloseLabel.Name = "AutoCloseLabel"
        Me.AutoCloseLabel.Size = New System.Drawing.Size(168, 17)
        Me.AutoCloseLabel.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label3.Location = New System.Drawing.Point(12, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "AutoClose List:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(12, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Tollerable CPU Use:"
        '
        'TollerableCpuUsageNumeric
        '
        Me.TollerableCpuUsageNumeric.Location = New System.Drawing.Point(154, 65)
        Me.TollerableCpuUsageNumeric.Name = "TollerableCpuUsageNumeric"
        Me.TollerableCpuUsageNumeric.Size = New System.Drawing.Size(51, 20)
        Me.TollerableCpuUsageNumeric.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Button1.Location = New System.Drawing.Point(11, 415)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CrucialCpuUsageNumeric)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 91)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(316, 100)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Crucial CPU use"
        '
        'CrucialCpuUsageNumeric
        '
        Me.CrucialCpuUsageNumeric.Location = New System.Drawing.Point(79, 62)
        Me.CrucialCpuUsageNumeric.Name = "CrucialCpuUsageNumeric"
        Me.CrucialCpuUsageNumeric.Size = New System.Drawing.Size(51, 20)
        Me.CrucialCpuUsageNumeric.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label4.Location = New System.Drawing.Point(6, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(304, 58)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Get notified when unwindowed processes use " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "more than"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 399)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(293, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "All changes won't be saved until pressing on the save button"
        Me.Label5.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.MinimizeRadioButton)
        Me.GroupBox2.Controls.Add(Me.CloseRadioButton)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 197)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(316, 70)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "When you hit the close button on the main form"
        '
        'MinimizeRadioButton
        '
        Me.MinimizeRadioButton.AutoSize = True
        Me.MinimizeRadioButton.Location = New System.Drawing.Point(12, 42)
        Me.MinimizeRadioButton.Name = "MinimizeRadioButton"
        Me.MinimizeRadioButton.Size = New System.Drawing.Size(97, 17)
        Me.MinimizeRadioButton.TabIndex = 1
        Me.MinimizeRadioButton.TabStop = True
        Me.MinimizeRadioButton.Text = "Minimize to tray"
        Me.MinimizeRadioButton.UseVisualStyleBackColor = True
        '
        'CloseRadioButton
        '
        Me.CloseRadioButton.AutoSize = True
        Me.CloseRadioButton.Location = New System.Drawing.Point(12, 19)
        Me.CloseRadioButton.Name = "CloseRadioButton"
        Me.CloseRadioButton.Size = New System.Drawing.Size(110, 17)
        Me.CloseRadioButton.TabIndex = 0
        Me.CloseRadioButton.TabStop = True
        Me.CloseRadioButton.Text = "Close the program"
        Me.CloseRadioButton.UseVisualStyleBackColor = True
        '
        'StartupCheckBox
        '
        Me.StartupCheckBox.AutoSize = True
        Me.StartupCheckBox.Location = New System.Drawing.Point(15, 273)
        Me.StartupCheckBox.Name = "StartupCheckBox"
        Me.StartupCheckBox.Size = New System.Drawing.Size(187, 17)
        Me.StartupCheckBox.TabIndex = 12
        Me.StartupCheckBox.Text = "Open program on windows startup"
        Me.StartupCheckBox.UseVisualStyleBackColor = True
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(340, 450)
        Me.Controls.Add(Me.StartupCheckBox)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TollerableCpuUsageNumeric)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.EditAutoCloseList)
        Me.Controls.Add(Me.AutoCloseLabel)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.EditWhiteList)
        Me.Controls.Add(Me.WhiteListLabel)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Settings"
        Me.Text = "Settings"
        CType(Me.TollerableCpuUsageNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.CrucialCpuUsageNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents WhiteListLabel As Label
    Friend WithEvents EditWhiteList As LinkLabel
    Friend WithEvents EditAutoCloseList As LinkLabel
    Friend WithEvents AutoCloseLabel As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TollerableCpuUsageNumeric As NumericUpDown
    Friend WithEvents Button1 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents CrucialCpuUsageNumeric As NumericUpDown
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents MinimizeRadioButton As RadioButton
    Friend WithEvents CloseRadioButton As RadioButton
    Friend WithEvents StartupCheckBox As CheckBox
End Class
