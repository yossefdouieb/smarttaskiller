﻿Public Class FadeOutTimer
    Inherits Timer
    Dim parent As Form

    Public Sub New()
        MyBase.New()
        parent = Nothing
        Me.Enabled = False
        Me.Interval = 55
    End Sub

    Public Sub New(p As Form)
        MyBase.New()
        parent = p
        Me.Enabled = False
        Me.Interval = 55
    End Sub

    Private Sub FadeOutTimer_Tick(sender As Object, e As EventArgs) Handles Me.Tick
        If IsNothing(parent) Then
            Throw New MissingFieldException("Dont Initiate this class without a parent form!")
        End If

        Dim workingAreaWidth As Integer = Screen.PrimaryScreen.WorkingArea.Width - parent.Width

        If Not parent.Location.X >= workingAreaWidth Then
            parent.Location = New Point(parent.Location.X + 30, parent.Location.Y)
        Else Me.Stop()
        End If

        parent.Refresh()
        If Not parent.Opacity <= 0.99 Then
            parent.Opacity -= 0.11
        End If
    End Sub
End Class
