﻿Public Class NotificationSystem

    Public YES_NO = 1
    Public OK = 2

    Public Function Notify(Type As Integer, Title As String, Content As String, Optional Time As Integer = Nothing) As Boolean
        Dim newNotification As New Notification
        Dim DialogRes As DialogResult

        If Not IsNothing(Time) And Time <> 0 Then
            newNotification.TimeLabel.Text = Time.ToString
        End If

        Select Case Type
            Case YES_NO
                newNotification.TitleLabel.Text = Title
                newNotification.ContentLabel.Text = Content
                DialogRes = newNotification.ShowDialog()
            Case OK
                newNotification.TitleLabel.Text = Title
                newNotification.ContentLabel.Text = Content
                newNotification.Controls.Remove(newNotification.Button1)
                newNotification.Button2.Text = "OK"
                newNotification.Button2.DialogResult = DialogResult.OK
                DialogRes = newNotification.ShowDialog()
        End Select

        If DialogRes = DialogResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
