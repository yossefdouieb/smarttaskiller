﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Notification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TitleLabel = New System.Windows.Forms.Label()
        Me.ContentLabel = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TimeLabel = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.FadeInTimer1 = New smarTasKiller.FadeInTimer()
        Me.FadeOutTimer1 = New smarTasKiller.FadeOutTimer()
        Me.SuspendLayout()
        '
        'TitleLabel
        '
        Me.TitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold)
        Me.TitleLabel.Location = New System.Drawing.Point(12, 9)
        Me.TitleLabel.Name = "TitleLabel"
        Me.TitleLabel.Size = New System.Drawing.Size(229, 45)
        Me.TitleLabel.TabIndex = 0
        Me.TitleLabel.Text = "Title"
        '
        'ContentLabel
        '
        Me.ContentLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.ContentLabel.Location = New System.Drawing.Point(12, 54)
        Me.ContentLabel.Name = "ContentLabel"
        Me.ContentLabel.Size = New System.Drawing.Size(229, 54)
        Me.ContentLabel.TabIndex = 1
        Me.ContentLabel.Text = "Content"
        '
        'Button1
        '
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Button1.Location = New System.Drawing.Point(176, 111)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Yes"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.No
        Me.Button2.Location = New System.Drawing.Point(247, 111)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(65, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "No"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TimeLabel
        '
        Me.TimeLabel.Font = New System.Drawing.Font("Arial", 35.0!)
        Me.TimeLabel.Location = New System.Drawing.Point(237, 9)
        Me.TimeLabel.Name = "TimeLabel"
        Me.TimeLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.TimeLabel.Size = New System.Drawing.Size(84, 67)
        Me.TimeLabel.TabIndex = 5
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'FadeInTimer1
        '
        Me.FadeInTimer1.Interval = 75
        '
        'FadeOutTimer1
        '
        Me.FadeOutTimer1.Interval = 55
        '
        'Notification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(322, 146)
        Me.ControlBox = False
        Me.Controls.Add(Me.TimeLabel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ContentLabel)
        Me.Controls.Add(Me.TitleLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Notification"
        Me.Opacity = 0R
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "YesNoNotification"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TitleLabel As Label
    Friend WithEvents ContentLabel As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents FadeInTimer1 As FadeInTimer
    Friend WithEvents FadeOutTimer1 As FadeOutTimer
    Friend WithEvents TimeLabel As Label
    Friend WithEvents Timer1 As Timer
End Class
