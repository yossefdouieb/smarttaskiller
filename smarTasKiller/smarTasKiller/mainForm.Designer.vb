﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea3 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend3 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.statusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.AutoRefreshList = New System.Windows.Forms.Timer(Me.components)
        Me.CheckProblematicProcesses = New System.Windows.Forms.Timer(Me.components)
        Me.TrayIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.TrayContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SleepToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinuteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinutesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SleepProgramTimer = New System.Windows.Forms.Timer(Me.components)
        Me.BW_AutoRefresh = New System.ComponentModel.BackgroundWorker()
        Me.BW_ProblematicProsesses = New System.ComponentModel.BackgroundWorker()
        Me.CpuChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RamChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.TimeChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        Me.TrayContextMenu.SuspendLayout()
        CType(Me.CpuChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.RamChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.TimeChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.statusLabel})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 428)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(830, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(42, 17)
        Me.ToolStripStatusLabel1.Text = "Status:"
        '
        'statusLabel
        '
        Me.statusLabel.Name = "statusLabel"
        Me.statusLabel.Size = New System.Drawing.Size(16, 17)
        Me.statusLabel.Text = "..."
        '
        'AutoRefreshList
        '
        Me.AutoRefreshList.Interval = 1000
        '
        'CheckProblematicProcesses
        '
        Me.CheckProblematicProcesses.Interval = 10000
        '
        'TrayIcon
        '
        Me.TrayIcon.BalloonTipText = "SmarTasKiller is running..."
        Me.TrayIcon.BalloonTipTitle = "SmarTasKiller"
        Me.TrayIcon.ContextMenuStrip = Me.TrayContextMenu
        Me.TrayIcon.Icon = CType(resources.GetObject("TrayIcon.Icon"), System.Drawing.Icon)
        Me.TrayIcon.Text = "SmarTasKiller"
        Me.TrayIcon.Visible = True
        '
        'TrayContextMenu
        '
        Me.TrayContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.SleepToolStripMenuItem, Me.ExitMenuItem})
        Me.TrayContextMenu.Name = "TrayContextMenu"
        Me.TrayContextMenu.Size = New System.Drawing.Size(117, 92)
        Me.TrayContextMenu.Text = "Exit"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.OpenToolStripMenuItem.Text = "Open"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'SleepToolStripMenuItem
        '
        Me.SleepToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MinuteToolStripMenuItem, Me.MinutesToolStripMenuItem, Me.HourToolStripMenuItem, Me.CustomToolStripMenuItem, Me.ResumeToolStripMenuItem})
        Me.SleepToolStripMenuItem.Name = "SleepToolStripMenuItem"
        Me.SleepToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.SleepToolStripMenuItem.Text = "Sleep"
        '
        'MinuteToolStripMenuItem
        '
        Me.MinuteToolStripMenuItem.Name = "MinuteToolStripMenuItem"
        Me.MinuteToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.MinuteToolStripMenuItem.Text = "1 Minute"
        '
        'MinutesToolStripMenuItem
        '
        Me.MinutesToolStripMenuItem.Name = "MinutesToolStripMenuItem"
        Me.MinutesToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.MinutesToolStripMenuItem.Text = "15 Minutes"
        '
        'HourToolStripMenuItem
        '
        Me.HourToolStripMenuItem.Name = "HourToolStripMenuItem"
        Me.HourToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.HourToolStripMenuItem.Text = "1 Hour"
        '
        'CustomToolStripMenuItem
        '
        Me.CustomToolStripMenuItem.Name = "CustomToolStripMenuItem"
        Me.CustomToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.CustomToolStripMenuItem.Text = "Until Resume / Restart"
        '
        'ResumeToolStripMenuItem
        '
        Me.ResumeToolStripMenuItem.Name = "ResumeToolStripMenuItem"
        Me.ResumeToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.ResumeToolStripMenuItem.Text = "Resume"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Name = "ExitMenuItem"
        Me.ExitMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.ExitMenuItem.Text = "Exit"
        '
        'SleepProgramTimer
        '
        '
        'BW_AutoRefresh
        '
        '
        'BW_ProblematicProsesses
        '
        '
        'CpuChart
        '
        ChartArea1.AxisX.MajorGrid.Enabled = False
        ChartArea1.AxisY.MajorGrid.Enabled = False
        ChartArea1.Name = "ChartArea1"
        Me.CpuChart.ChartAreas.Add(ChartArea1)
        Me.CpuChart.Dock = System.Windows.Forms.DockStyle.Fill
        Legend1.Name = "Legend1"
        Me.CpuChart.Legends.Add(Legend1)
        Me.CpuChart.Location = New System.Drawing.Point(3, 3)
        Me.CpuChart.Name = "CpuChart"
        Series1.ChartArea = "ChartArea1"
        Series1.Color = System.Drawing.Color.Lime
        Series1.IsValueShownAsLabel = True
        Series1.Label = "#VAL{N2}%"
        Series1.Legend = "Legend1"
        Series1.Name = "cpu"
        Me.CpuChart.Series.Add(Series1)
        Me.CpuChart.Size = New System.Drawing.Size(816, 396)
        Me.CpuChart.TabIndex = 0
        Me.CpuChart.Text = "Chart1"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.HotTrack = True
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(830, 428)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.CpuChart)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(822, 402)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cpu Chart"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label1.Size = New System.Drawing.Size(816, 396)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "No information available"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.RamChart)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(822, 402)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Ram Chart"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(816, 396)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "No information available"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RamChart
        '
        ChartArea2.AxisX.MajorGrid.Enabled = False
        ChartArea2.AxisY.MajorGrid.Enabled = False
        ChartArea2.Name = "ChartArea1"
        Me.RamChart.ChartAreas.Add(ChartArea2)
        Me.RamChart.Dock = System.Windows.Forms.DockStyle.Fill
        Legend2.Name = "Legend1"
        Me.RamChart.Legends.Add(Legend2)
        Me.RamChart.Location = New System.Drawing.Point(3, 3)
        Me.RamChart.Name = "RamChart"
        Series2.ChartArea = "ChartArea1"
        Series2.IsValueShownAsLabel = True
        Series2.Label = "#VAL{D}MB"
        Series2.Legend = "Legend1"
        Series2.Name = "ram"
        Me.RamChart.Series.Add(Series2)
        Me.RamChart.Size = New System.Drawing.Size(816, 396)
        Me.RamChart.TabIndex = 2
        Me.RamChart.Text = "Chart2"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.CheckedListBox1)
        Me.TabPage3.Controls.Add(Me.TimeChart)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(822, 402)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Usage over time chart"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.CheckOnClick = True
        Me.CheckedListBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(3, 399)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(180, 0)
        Me.CheckedListBox1.Sorted = True
        Me.CheckedListBox1.TabIndex = 4
        '
        'TimeChart
        '
        ChartArea3.AxisX.MajorGrid.Enabled = False
        ChartArea3.BorderWidth = 5
        ChartArea3.Name = "ChartArea1"
        Me.TimeChart.ChartAreas.Add(ChartArea3)
        Me.TimeChart.Dock = System.Windows.Forms.DockStyle.Right
        Legend3.Name = "Legend1"
        Me.TimeChart.Legends.Add(Legend3)
        Me.TimeChart.Location = New System.Drawing.Point(-453, 399)
        Me.TimeChart.Name = "TimeChart"
        Me.TimeChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright
        Me.TimeChart.Size = New System.Drawing.Size(636, 0)
        Me.TimeChart.TabIndex = 3
        Me.TimeChart.Text = "Chart1"
        '
        'Label4
        '
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Label4.Location = New System.Drawing.Point(183, 399)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(636, 0)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Choose app to start"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(816, 396)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "No information available"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(830, 450)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "MainForm"
        Me.Text = "smarTasKiller"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TrayContextMenu.ResumeLayout(False)
        CType(Me.CpuChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.RamChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.TimeChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents statusLabel As ToolStripStatusLabel
    Friend WithEvents AutoRefreshList As Timer
    Friend WithEvents CheckProblematicProcesses As Timer
    Friend WithEvents TrayIcon As NotifyIcon
    Friend WithEvents TrayContextMenu As ContextMenuStrip
    Friend WithEvents ExitMenuItem As ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SleepToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MinuteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MinutesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HourToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CustomToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SleepProgramTimer As Timer
    Friend WithEvents ResumeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BW_AutoRefresh As System.ComponentModel.BackgroundWorker
    Friend WithEvents BW_ProblematicProsesses As System.ComponentModel.BackgroundWorker
    Friend WithEvents CpuChart As DataVisualization.Charting.Chart
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents RamChart As DataVisualization.Charting.Chart
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TimeChart As DataVisualization.Charting.Chart
    Friend WithEvents CheckedListBox1 As CheckedListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
End Class
