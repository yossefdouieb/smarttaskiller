﻿Public Class Settings

    Dim NewWhiteList As New List(Of String)
    Dim NewAutoCloseList As New List(Of String)

    Private Sub Settings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        StartupCheckBox.Checked = My.Settings.StartupStart
        If My.Settings.MinimizeToTrayOnClose = True Then
            MinimizeRadioButton.Checked = True
        Else
            CloseRadioButton.Checked = True
        End If
        NewWhiteList = My.Settings.whiteList.Cast(Of String).ToList
        NewAutoCloseList = My.Settings.AlwaysCloseList.Cast(Of String).ToList
        WhiteListLabel.Text = String.Join(", ", My.Settings.whiteList.Cast(Of String)().ToArray())
        AutoCloseLabel.Text = String.Join(", ", My.Settings.AlwaysCloseList.Cast(Of String)().ToArray())
        TollerableCpuUsageNumeric.Value = My.Settings.TolleratedCpuUse
        CrucialCpuUsageNumeric.Value = My.Settings.CrucialCpuUse
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        My.Settings.CrucialCpuUse = CrucialCpuUsageNumeric.Value
        My.Settings.TolleratedCpuUse = TollerableCpuUsageNumeric.Value
        My.Settings.whiteList.Clear()
        My.Settings.whiteList.AddRange(NewWhiteList.ToArray)
        My.Settings.AlwaysCloseList.Clear()
        My.Settings.AlwaysCloseList.AddRange(NewAutoCloseList.ToArray)
        My.Settings.Save()
    End Sub

    Public Function OpenListEditor(list As String()) As List(Of String)
        Dim myListEditor As New ListEditor
        Dim newList As New List(Of String)

        For Each Str As String In list
            myListEditor.ListBox1.Items.Add(Str)
        Next

        If myListEditor.ShowDialog() = DialogResult.OK Then
            For Each Str As String In myListEditor.ListBox1.Items
                newList.Add(Str)
            Next
        End If
        Return newList
    End Function

    Private Sub EditWhiteList_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles EditWhiteList.LinkClicked
        NewWhiteList = OpenListEditor(NewWhiteList.ToArray())
        WhiteListLabel.Text = String.Join(", ", NewWhiteList.ToArray())
    End Sub

    Private Sub EditAutoCloseList_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles EditAutoCloseList.LinkClicked
        NewAutoCloseList = OpenListEditor(NewAutoCloseList.ToArray())
        AutoCloseLabel.Text = String.Join(", ", NewAutoCloseList.ToArray())
    End Sub

    Private Sub CloseRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles CloseRadioButton.CheckedChanged
        If CloseRadioButton.Checked Then My.Settings.MinimizeToTrayOnClose = False
    End Sub

    Private Sub MinimizeRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles MinimizeRadioButton.CheckedChanged
        If MinimizeRadioButton.Checked Then My.Settings.MinimizeToTrayOnClose = True
    End Sub

    Private Sub StartupCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles StartupCheckBox.CheckedChanged
        If StartupCheckBox.Checked Then
            Try
                My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).SetValue(Application.ProductName, Application.ExecutablePath)
                My.Settings.StartupStart = True
            Catch ex As Exception
                MsgBox("Error!, Reason: " + ex.Message, MsgBoxStyle.Exclamation)
            End Try
        Else
            Try
                My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).DeleteValue(Application.ProductName)
                My.Settings.StartupStart = False
            Catch ex As Exception
                MsgBox("Error!, Reason: " + ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
    End Sub
End Class