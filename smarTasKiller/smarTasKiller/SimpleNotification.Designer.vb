﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SimpleNotification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TitleLabel = New System.Windows.Forms.Label()
        Me.ContentLabel = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.FadeInTimer1 = New smarTasKiller.FadeInTimer()
        Me.FadeOutTimer1 = New smarTasKiller.FadeOutTimer()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TitleLabel
        '
        Me.TitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold)
        Me.TitleLabel.Location = New System.Drawing.Point(12, 9)
        Me.TitleLabel.Name = "TitleLabel"
        Me.TitleLabel.Size = New System.Drawing.Size(218, 18)
        Me.TitleLabel.TabIndex = 0
        Me.TitleLabel.Text = "Title"
        '
        'ContentLabel
        '
        Me.ContentLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.ContentLabel.Location = New System.Drawing.Point(12, 31)
        Me.ContentLabel.Name = "ContentLabel"
        Me.ContentLabel.Size = New System.Drawing.Size(199, 84)
        Me.ContentLabel.TabIndex = 1
        Me.ContentLabel.Text = "Content"
        '
        'Button2
        '
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Button2.Location = New System.Drawing.Point(219, 92)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(65, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "OK"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'FadeInTimer1
        '
        Me.FadeInTimer1.Interval = 75
        '
        'FadeOutTimer1
        '
        Me.FadeOutTimer1.Interval = 55
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 55.0!)
        Me.Label1.Location = New System.Drawing.Point(217, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 82)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "0"
        '
        'SimpleNotification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(296, 127)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ContentLabel)
        Me.Controls.Add(Me.TitleLabel)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SimpleNotification"
        Me.Opacity = 0R
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "SimpleNotification"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TitleLabel As Label
    Friend WithEvents ContentLabel As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents FadeInTimer1 As FadeInTimer
    Friend WithEvents FadeOutTimer1 As FadeOutTimer
    Friend WithEvents Label1 As Label
End Class
