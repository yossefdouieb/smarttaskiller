SmartTasKiller

A program that monitors running processes and display usage statistics.
The program alerts you if you closed a program's windows (GUI) but some processes remain unclosed.

Alert notification demo:

![alt text](https://gitlab.com/yossefdouieb/smarttaskiller/-/raw/master/AnimationAlert.gif)


Processes Stats demo:

![alt text](https://gitlab.com/yossefdouieb/smarttaskiller/-/raw/master/AnimationStats.gif)

